from django.contrib import admin

# Register your models here.
from .models  import *

admin.site.register(bus)
admin.site.register(bus_route)
admin.site.register(station)
admin.site.register(tickets)