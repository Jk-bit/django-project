from django.db import models
#from login_page import models as login_page_models
from login_page.models import * 
# Create your models here
import datetime
from django.utils import timezone

class bus(models.Model) :
    bus_id = models.CharField(primary_key=True, blank=False, max_length=6)
    date = models.DateTimeField(blank=True)
    source = models.CharField(blank=False, max_length=100)
    destination = models.CharField(blank=False, max_length=100)

class bus_route(models.Model) :
    bus_id = models.CharField( max_length=6)
    station_code = models.ForeignKey('station', on_delete=models.CASCADE)
    arrival_time = models.TimeField(blank=False, max_length=6)
    fare_from_source = models.PositiveSmallIntegerField(blank=False)

class station(models.Model) :
    station_code = models.CharField(primary_key=True, max_length=5)
    station_name = models.CharField(blank=False, max_length=100)

class tickets(models.Model) :
    user_id = models.CharField(max_length=19, blank=False)
    bus_id = models.CharField(max_length=6, blank=False)
    date = models.DateTimeField(blank=False)
    seat = models.IntegerField(blank=False, default="1")
    source = models.CharField(blank=False, max_length=100)
    destination = models.CharField(blank=False, max_length=100)
    fare = models.IntegerField(blank=False)




