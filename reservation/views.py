from django.shortcuts import render
from .forms import *
from .models import *
from django.contrib import messages
from django.shortcuts import redirect
from django.db import connection
from django import forms
import datetime
# Create your views here

def reservation_page(request) :
    my_form = reservation_form(request.POST)
    if(request.method == 'POST') :
        my_form = reservation_form(request.POST)
        if(my_form.is_valid()) :
            #Queries
            global source
            global destination
            name = my_form.cleaned_data.get('name')
            email = my_form.cleaned_data.get('email')
            no_of_passengers = my_form.cleaned_data.get('no_passengers')
            date_time = my_form.cleaned_data.get('date_time')
            source = my_form.cleaned_data.get('source')
            destination = my_form.cleaned_data.get('destination')
            date = my_form.cleaned_data.get('date_time')
            source_code = station.objects.get(station_name=source).station_code
            destination_code = station.objects.get(station_name=destination).station_code
            with connection.cursor() as cursor :
            #cursor.execute('SELECT A.bus_id, A.fare_from_source, B.fare_from_source, A.arrival_time, B.arrival_time  from (reservation_bus_route as A natural join reservation_bus_route as B) where A.station_code_id=%s or B.station_code_id=%s', [source_code, destination_code])
                cursor.execute('SELECT A.fare_from_source, B.fare_from_source from reservation_bus_route as A,reservation_bus_route as B where A.bus_id = B.bus_id and A.station_code_id=%s and B.station_code_id=%s', [source_code, destination_code])
                fare_check = cursor.fetchall()
            if(date < datetime.date.today()) :
                messages.add_message(request, messages.ERROR,"Enter valid date")
            elif(source == destination) :
                messages.add_message(request, messages.ERROR, "Source and destination can't be same")

            
            else :
                global row
                row = bus_choice(request)
                global info
                info = [name, email, no_of_passengers, date_time, source, destination]
                return redirect('../available')
        else :
            print("No")
            
    else :
        print("Nope")

    return render(request, "reservation.html", {'form' : my_form})

def home_page(request, user_data) :
    global user_id
    user_id = user_data
    return render(request, "index.html", {'user_id' : user_data})

def my_tickets(request) :
    with connection.cursor() as cursor :
        cursor.execute('SELECT * FROM reservation_tickets where user_id=%s', [user_id])
        rows = cursor.fetchall()
    before = []
    after = []
    r = []
    for i in range(len(rows)) :
        r.append(list(rows[i]))
        obj = bus.objects.get(bus_id=rows[i][1])
        r[i].append(obj.source)
        r[i].append(obj.destination)
        s_id = station.objects.get(station_name=rows[i][3]).station_code
        d_id = station.objects.get(station_name=rows[i][4]).station_code
        with connection.cursor() as cursor :
            cursor.execute('SELECT arrival_time from reservation_bus_route where bus_id=%s AND station_code_id=%s', [r[i][1], s_id])
            arr_time = cursor.fetchall()
        with connection.cursor() as cursor :
            cursor.execute('SELECT arrival_time from reservation_bus_route where bus_id=%s AND station_code_id=%s', [r[i][1], d_id])
            d_time = cursor.fetchall()
        r[i].append(list(arr_time[0]))
        r[i].append(list(d_time[0]))
    #TODO render the data
    return render(request, "my_tickets.html", {'rows' : r})


def bus_choice( request) :
        source_code = station.objects.get(station_name=source).station_code
        destination_code = station.objects.get(station_name=destination).station_code
        with connection.cursor() as cursor :
            #cursor.execute('SELECT A.bus_id, A.fare_from_source, B.fare_from_source, A.arrival_time, B.arrival_time  from (reservation_bus_route as A natural join reservation_bus_route as B) where A.station_code_id=%s or B.station_code_id=%s', [source_code, destination_code])
            cursor.execute('SELECT A.bus_id, A.fare_from_source, B.fare_from_source, A.arrival_time, B.arrival_time from reservation_bus_route as A,reservation_bus_route as B where A.bus_id = B.bus_id and A.station_code_id=%s and B.station_code_id=%s', [source_code, destination_code])
            row = cursor.fetchall()
        return row

def available(request) :
    """request.method = "POST"
    my_forms = []
    check = []
    for choice in row :
        choice = list(choice)
        fare = choice[1] - choice[2]
        arrival_time = choice[3] 
        aboarding_time = choice[4]
        bus_id = choice[0]
        check.append([bus_id, fare, arrival_time, aboarding_time, source, destination])
        my_forms.append(choice_form(request.POST, ))
    for my_form in my_forms :
            if(((my_form.is_valid()))) :
                tick = my_form.cleaned_data.get('choice')
                print(tick)
            else :
                print("What")
    #else :
    #    print("how")

    data = zip(my_forms, check)
    return render(request, 'available1.html', {'zipped' : data})   """
    row = bus_choice(request)
    my_form = choice_form(request.POST)
    seat_form = seat_choose(request.POST)
    l = [list(x) for x in row]
    seat = []
    data = []
    i = 0
    j = 0
    while(1) :
        if (j == len(row)) :
            break
        bus_id = l[i][0]
        fare = l[i][2] - l[i][1]
        if (fare < 0) :
            j += 1
            l.pop(i)
            continue
        arrival_time = l[i][3]
        aboarding_time = l[i][4]
        starting_point = bus.objects.get(bus_id=bus_id).source
        end_point = bus.objects.get(bus_id=bus_id).destination
        l[i] = (str(i), str(bus_id) + ",     Fare: " + str(fare) + ",    Arrival Time: " + str(arrival_time) + ",    Aboarding Time: " + str(aboarding_time) + ",    Starting point: " + str(starting_point) + ",     End Point: " + str(end_point)) 
        i += 1
        j += 1
    row = l
    my_form.fields['choice'] = forms.Field(widget=forms.RadioSelect(choices=row), required=False)
    seat_choices = [(str(i), i) for i in range(1, 36)]
    with connection.cursor() as cursor :
        cursor.execute('SELECT seat from reservation_tickets where bus_id=%s', [bus_id])
        seat_taken = cursor.fetchall()
    for i in seat_taken :
        try :
            seat_choices.remove((str(i[0]), i[0]))
        except :
            pass
    for i in range(int(info[2])) :
        seat_form.fields["seat" + str(i)] = forms.IntegerField(widget=forms.Select(choices=seat_choices))
    if(request.method == "POST") :
        if(my_form.is_valid() and seat_form.is_valid()) :
            print("Mumbai Indians")
        else :
            value = my_form.cleaned_data.get('choice')
            for i in range(int(info[2])) :
                seat.append(seat_form.cleaned_data.get("seat" + str(i)))
            row = (row[int(value)][1])
            if(set(seat) != seat) :
                messages.add_message(request, messages.ERROR, "Enter different seat numbers to different passenger")
            else :    
                return ticket_page(request, row, info, seat)
    else :
        print("CSK")
    return render(request, "available.html", {'form' : my_form, 'seat' : seat_form}) 

def ticket_page(request, data, info, seat) :
    data = data.split(',    ')
    for i in range(1, len(data)) :
        if(i == 2 or i == 3) :
            d = str(data[i].split(':')[1]) + ":" + str(data[i].split(':')[2]) + ":" + str(data[i].split(':')[3])
            data[i] = d
        else :
            data[i] = str(data[i].split(':')[1])
    data[1] = int(data[1]) * int(info[2])
    for s in seat :
        obj = tickets.objects.create(user_id=user_id, bus_id=data[0], date=info[3], source=info[4], destination=info[5], fare=int(data[1]), seat=int(s))
    obj.save()
    info.append(data[2][1:])
    info.append(data[3][1:])
    print(info)
    return render(request, "ticket.html", {'data' : data, 'info' : info, 'seat' : seat})




    
   