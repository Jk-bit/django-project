from django.shortcuts import render
from django.db import connection
from .forms import *
from django import forms
from reservation.models import station as s
from .models import *
# Create your views here.

def station_info(request) :
    with connection.cursor() as cursor :
        cursor.execute('SELECT * from reservation_station')
        row = cursor.fetchall()
    print(row)

    return render(request, "stations.html", {'data' : row})

def admin_home_page(request, admin_id) :
    return render(request, "admin_index.html", {})

def employee_information(request) :
    my_form = station_select(request.POST)
    my_form.fields['station'] = forms.ModelChoiceField(queryset=s.objects.values_list('station_name', flat=True), to_field_name='station_name')
    if(request.method == 'POST') :
        if(my_form.is_valid()) :
            station = my_form.cleaned_data.get('station') 
            station_code = s.objects.get(station_name=station)
            print(station_code)
            with connection.cursor() as cursor :
                cursor.execute('SELECT * from station_employee_info where station_code_id=%s', [station_code.station_code])
                row = cursor.fetchall()
            return render(request, "employee_info.html", {'data' : row, 'form' : my_form})
    
    return render(request, "employee_info.html", {'form' : my_form})

def salary_info(request) :
    with connection.cursor() as cursor :
        cursor.execute('SELECT * from station_salary')
        row = cursor.fetchall()
    return render(request, "salary_info.html", {'data' : row})

        