from django.db import models
from reservation import models as m
# Create your models herre

class employee_info(models.Model) :
    employee_id = models.PositiveIntegerField(blank=False, primary_key=True)
    employee_name = models.CharField(blank=False, max_length=30)
    designation = models.ForeignKey('salary', on_delete=models.CASCADE)
    dob = models.DateField(blank=False)
    station_code = models.ForeignKey(m.station, blank=False, on_delete=models.CASCADE)

class on_duty_employee(models.Model) :
    bus_id = models.ForeignKey(m.bus, on_delete=models.CASCADE)
    date = models.DateField(blank=False)
    employee_id = models.ForeignKey('employee_info', on_delete=models.CASCADE)

    class Meta :
        unique_together = (('bus_id', 'date'))

class salary(models.Model) :
    designation = models.CharField(primary_key=True, max_length=19, blank=False)
    salary = models.PositiveIntegerField(blank=False)