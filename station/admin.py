from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(employee_info)
admin.site.register(on_duty_employee)
admin.site.register(salary)