from django import forms
from .models import *

class login_form(forms.Form) :
    user_id = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())

class signin_form(forms.ModelForm) :
    class Meta :
        model = passenger
        fields = [
            "user_id",
            "name", 
            "email_id",
            "password",
        ]
        widgets = {
            'password' : forms.PasswordInput()
        }

class admin_login_form(forms.Form) :
    admin_id = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())

class admin_signin_form(forms.Form) :
    class Meta :
        fields = [
            'admin_id',
            'name',
            'email_id',
            'password',
        ]

        widgets = {
            'password' : forms.PasswordInput()
        }

        



    """def clean_user_id(self) :
        super(signin_form, self).clean()

        user_id = self.cleaned_data.get('user_id', '')

        if (user_id in passenger.objects.raw('SELECT user_id from login_page_passenger')) :
            raise ValidationError("Please enter another user_id")
        else :
            return user_id """