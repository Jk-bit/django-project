from django.shortcuts import render
from django.contrib import messages
from django.shortcuts import redirect
from .forms import *
from .models  import *
import time
from reservation import views
from station import views as v
# Create your views here.


def login_page(request) :
    try :
        del request.session['user']
    except :
        pass
    my_form = login_form(request.POST)
    if(request.method == "POST") :
        my_form = login_form(request.POST)
        if(my_form.is_valid()) :
            user_id = my_form.cleaned_data.get("user_id")
            password = my_form.cleaned_data.get("password")
            if(passenger.objects.filter(user_id=user_id).exists()) :
                if(password == passenger.objects.get(user_id=user_id).password) :
                    #return render(request, "index.html", {})
                    request.session['user'] = user_id
                    return views.home_page(request, user_id)
                else :
                    messages.add_message(request, messages.ERROR, "Wrong Password")
            else :
                messages.add_message(request, messages.ERROR, "User Name is taken")
        else :
            my_form = login_form()
    else :
        pass
   
    context = {
        "form" :my_form
    }
    return render(request, "login.html", context)

#def home_page(request) :
   
#    return render(request, "index.html", {})

def signin_page(request) :
    my_form = signin_form(request.POST)

    if(request.method == "POST") :
        if(my_form.is_valid()) :
            my_form.save()
            my_form = signin_form()
            messages.add_message(request, messages.SUCCESS, "Signed in successfully")
            time.sleep(3)
            return redirect('../')
        else :
            pass
    else :
        print("No")
        my_form.full_clean()
    context = {
        "form" : my_form
    }

    return render(request, "signin.html", context)

def admin_login(request) :
    try :
        del request.session['admin']
    except :
        pass
    my_form = admin_login_form(request.POST)
    if(request.method == "POST") :
        my_form = admin_login_form(request.POST)
        if(my_form.is_valid()) :
            admin_id = my_form.cleaned_data.get("admin_id")
            password = my_form.cleaned_data.get("password")
            if(admins.objects.filter(admin_id=admin_id).exists()) :
                if(password == admins.objects.get(admin_id=admin_id).password) :
                    #return render(request, "index.html", {})
                    request.session['admin'] = admin_id
                    return v.admin_home_page(request, admin_id)
                else :
                    messages.add_message(request, messages.ERROR, "Wrong Password")
            else :
                messages.add_message(request, messages.ERROR, "User Name is taken")
        else :
            my_form = admin_login_form()
    else :
        pass
   
    context = {
        "form" :my_form
    }
    return render(request, "admin_login.html", context)
   

