"""bus_management URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from login_page.views import *
from reservation.views import *
from station.views import *
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', login_page),
    path('signin/', signin_page), 
    path('index/', home_page), 
    path('reservation/', reservation_page) ,
    path('available/', available),
    path('ticket/', ticket_page), 
    path('my_tickets', my_tickets),
    path('stations', station_info), 
    path('admins/',admin_login), 
    path('admin_index', admin_home_page),
    path('employee_info/', employee_information),
    path('salary', salary_info),
    

]
